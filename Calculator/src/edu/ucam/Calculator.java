package edu.ucam;

public class Calculator {

	static double num1 = 15, num2 = 5;
	static String cadena1 = "Calculadora";
	static String cadena2 = "Probando";
		
	public static void main(String[] args) {
		
		System.out.println("Operaciones con calculadora: \n");
		System.out.println("El resultado de la suma: " + suma(num1, num2));
		System.out.println("El resultado de la resta: " + resta(num1, num2));
		System.out.println("El resultado de la multiplicación: " + multiplicacion(num1, num2));
		System.out.println("El resultado de la división: " + division(num1, num2));
		System.out.println("Cadenas de texto: " + cadena1 + " " + cadena2);
	}
		
	public Calculator() {
	}

	public static double suma(double x, double y) {
		return x + y;
	}

	public static double resta(double x, double y) {
		return x - y;
	}
	
	public static double multiplicacion(double x, double y) {
		return x * y;
	}
		
	public static double division(double x, double y) {
		return x / y;
	}

	public static boolean menorQue(double x, double y){
		return x < y;
	}
		
	public static boolean mayorQue(double x, double y){
		return x > y;
	}
	
	public static boolean igualQue(double x, double y){
		return x == y;
	}
}
