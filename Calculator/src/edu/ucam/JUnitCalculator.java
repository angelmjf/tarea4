package edu.ucam;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class JUnitCalculator extends TestCase {
	double n1 = 15, n2 = 5;
	String cadena1 = "Calculadora";
	String cadena2 = "Probando";
	
	Calculator calculator = new Calculator();
	
	@Before
	public void antes(){
		System.out.println("Inicio del Test");
	}
	
	@After	
	public void despues(){
		System.out.println("Fin del Test");
	}
		
	@Test
	public void testSuma(){
		System.out.println("Resultado " + n1 + " + " + n2 + " = " + (n1+n2));
		double resultado = Calculator.suma(n1, n2);
		double esperado = 20; 
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testResta(){
		System.out.println("Resultado " + n1 + " - " + n2 + " = " + (n1-n2));
	    double resultado = Calculator.resta(n1, n2);
		double esperado = 10; 
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testMultiplicacion(){
		System.out.println("Resultado " + n1 + " * " + n2 + " = " + (n1*n2));
	    double resultado = Calculator.multiplicacion(n1, n2);
		double esperado = 75;  
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testDivision(){
		System.out.println("Resultado " + n1 + " * " + n2 + " = " + (n1/n2));
	    double resultado = Calculator.division(n1, n2);
		double esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	public void testMenorQue(){
		System.out.println("Resultado " + n1 + " < " + n2);
		assertTrue(Calculator.menorQue(n1, n2));
		assertFalse(!Calculator.menorQue(n1, n2));
	}
	
	public void testMayorQue(){
		System.out.println("Resultado " + n1 + " > " + n2);
		assertTrue(Calculator.mayorQue(n1, n2));
		assertFalse(!Calculator.mayorQue(n1, n2));
	}
	
	@Test	
	public void testCadena(){
		System.out.println(cadena1 + " " + n1);
		assertNotSame("No son tipos de datos iguales", cadena1, n1);
	}
	
	@Test
	public void testCadenaNull() {
		System.out.println(cadena1);
		assertNull("Nulo", cadena1);
	}
	
	@Test
	public void testCadenaNotNull() {
		System.out.println(cadena1);
		assertNotNull("No Nulo", cadena2);
	}

}


